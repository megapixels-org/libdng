.. LibDNG documentation master file, created by
   sphinx-quickstart on Tue Nov 28 16:40:51 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LibDNG's documentation!
=======================

The libdng library is a wrapper around libtiff in order to easily produce TIFF
files that are following the DNG 1.4 specification.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   api